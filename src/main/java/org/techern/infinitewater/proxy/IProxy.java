package org.techern.infinitewater.proxy;

import net.minecraft.item.Item;

/**
 * An interface to be used by {@link ServerProxy} and {@link ClientProxy}
 *
 * @since 2.0.0
 */
public interface IProxy {

    /**
     * Registers an {@link Item} in the {@link net.minecraft.client.renderer.ItemModelMesher}
     *
     * @param item The {@link Item} being registered
     * @param metadata The metadata of said item
     * @param itemName The item's name
     *
     * @since 2.0.0
     */
    void registerItemModelMesher(Item item, int metadata, String itemName);

    /**
     * Registers an {@link Item} in the {@link net.minecraft.client.renderer.ItemModelMesher}
     *
     * @param item     The {@link Item} being registered
     * @param metadata The metadata of said item
     * @param itemName The item's name
     * @param metadataNameAndValue The name and value of the metadata
     *
     * @since 2.0.0
     */
     void registerItemModelMesher(Item item, int metadata, String itemName, String metadataNameAndValue);
}
