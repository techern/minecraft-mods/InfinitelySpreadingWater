package org.techern.infinitewater.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.TickPriority;
import net.minecraft.world.World;
import net.minecraftforge.common.ToolType;
import org.techern.infinitewater.SpreadingUtilities;

import javax.annotation.Nullable;
import java.util.Random;

/**
 * A {@link Block} for infinitely spreading water
 *
 * @since 1.0.0
 */
public class BlockInfinitelySpreadingWater extends Block {

    /**
     * Creates a new {@link BlockInfinitelySpreadingWater}
     *
     * @since 1.0.0
     */
    public BlockInfinitelySpreadingWater() {
        super(Block.Properties.create(Material.MISCELLANEOUS, MaterialColor.WATER).noDrops()
                .hardnessAndResistance(0.1f, 0.1f)
                .harvestLevel(0).harvestTool(ToolType.SHOVEL)
                .lightValue(2)); //No drops to prevent someone mining the block and placing a hundred more
        this.setRegistryName("infinitely_spreading_water");
    }

    /**
     * Called on a tick
     *
     * @param state The current {@link BlockState}
     * @param worldIn The {@link World} we're in
     * @param pos The {@link BlockPos} of this {@link BlockInfinitelySpreadingWater}
     * @param rand The {@link Random} attach to the tick
     *
     * @since 2.0.0
     */
    @Override
    public void tick(BlockState state, World worldIn, BlockPos pos, Random rand) {

        SpreadingUtilities.processBlockTick(state, worldIn, pos, rand);
    }

    /**
     * Called when this {@link BlockInfinitelySpreadingWater} is placed
     *
     * @param worldIn The {@link World}
     * @param pos The {@link BlockPos}
     * @param state The {@link BlockState}
     * @param placer The {@link LivingEntity} that placed this {@link BlockInfinitelySpreadingWater}
     * @param stack The {@link ItemStack} used to place
     */
    @Override
    public void onBlockPlacedBy(World worldIn, BlockPos pos, BlockState state, @Nullable LivingEntity placer, ItemStack stack) {
        super.onBlockPlacedBy(worldIn, pos, state, placer, stack);

        worldIn.getPendingBlockTicks().scheduleTick(pos, this, 100, TickPriority.HIGH);
    }
}
