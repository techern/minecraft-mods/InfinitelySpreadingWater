package org.techern.infinitewater.items;

import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.*;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.TickPriority;
import net.minecraft.world.World;
import org.techern.infinitewater.InfinitelySpreadingWaterMod;

import javax.annotation.Nullable;


/**
 * An {@link Item} used to place a {@link org.techern.infinitewater.blocks.BlockInfinitelySpreadingWater}
 *
 * @since 1.1.0
 */
public class ItemInfinitelySpreadingWaterBucket extends Item {


    /**
     * Creates a new {@link ItemInfinitelySpreadingWaterBucket}
     *
     * @since 1.1.0
     */
    public ItemInfinitelySpreadingWaterBucket() {
        super(new Item.Properties().maxStackSize(1).setNoRepair().group(ItemGroup.TOOLS));

        this.setRegistryName("infinitely_spreading_water_bucket");

    }

    @Override
    public ActionResultType onItemUse(ItemUseContext context) {
        return placeBlock(context.getPlayer(), context.getWorld(), context.getPos().offset(context.getFace())) ? ActionResultType.SUCCESS : ActionResultType.FAIL;
    }

    boolean placeBlock(@Nullable PlayerEntity player, World worldIn, BlockPos posIn) {

        BlockState state = worldIn.getBlockState(posIn);
        Material material = state.getMaterial();
        boolean isMaterialLiquidOrGas = !material.isSolid();

        if (!worldIn.isAirBlock(posIn) && !isMaterialLiquidOrGas) {
            return false;
        } else {

            if (!worldIn.isRemote && (isMaterialLiquidOrGas) && !material.isLiquid()) {
                worldIn.destroyBlock(posIn, true);
            }

            worldIn.playSound(player, posIn, SoundEvents.ITEM_BUCKET_EMPTY, SoundCategory.BLOCKS, 1.0F, 1.0F);
            worldIn.setBlockState(posIn, InfinitelySpreadingWaterMod.INFINITELY_SPREADING_WATER_BLOCK.getDefaultState(), 11);

            worldIn.getPendingBlockTicks().scheduleTick(posIn, InfinitelySpreadingWaterMod.INFINITELY_SPREADING_WATER_BLOCK, 100, TickPriority.HIGH);


            return true;
        }
    }


}


