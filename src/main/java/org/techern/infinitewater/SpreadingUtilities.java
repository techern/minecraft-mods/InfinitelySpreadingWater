package org.techern.infinitewater;

import net.minecraft.block.BlockState;
import net.minecraft.block.FlowingFluidBlock;
import net.minecraft.fluid.Fluids;
import net.minecraft.fluid.WaterFluid;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.TickPriority;
import net.minecraft.world.World;
import org.techern.infinitewater.blocks.BlockInfinitelySpreadingWater;

import java.util.Random;

/**
 * Separates some of the logic from blocks and items so we use the same code less
 *
 * @since 2.0.0
 */
public class SpreadingUtilities {


    /**
     * Process a block tick
     *
     * @param state The current {@link BlockState}
     * @param worldIn The {@link World} we're in
     * @param pos The {@link BlockPos} of this {@link BlockInfinitelySpreadingWater}
     * @param rand The {@link Random} attach to the tick
     *
     * @since 2.0.0
     */
    public static void processBlockTick(BlockState state, World worldIn, BlockPos pos, Random rand) {

        //Let's expand
        expand(worldIn, pos.north(), state);
        expand(worldIn, pos.south(), state);
        expand(worldIn, pos.east(), state);
        expand(worldIn, pos.west(), state);

        worldIn.setBlockState(pos, Fluids.WATER.getDefaultState().getBlockState());
    }



    /**
     * Attempt to expand by just replacing air
     *
     * @param world The {@link World}
     * @param position The {@link BlockPos} to replace
     *
     * @since 1.0.0
     */
    public static void expand(World world, BlockPos position, BlockState state) {

        BlockState targetState = world.getBlockState(position);

        if (world.isAirBlock(position)) {
            world.setBlockState(position, state.getBlockState());
            world.getPendingBlockTicks().scheduleTick(position, state.getBlock(), 10, TickPriority.HIGH);
        } else if (targetState.getBlock() instanceof FlowingFluidBlock) {
            if (!targetState.getFluidState().getFluid().isSource(targetState.getFluidState())) { //This should automatically reject source water blocks as they are just Fluids.WATER
                world.setBlockState(position, state.getBlockState());
                world.getPendingBlockTicks().scheduleTick(position, state.getBlock(), 10, TickPriority.HIGH);
            }
        } else if (!targetState.isSolid()) {
            world.destroyBlock(position, true);
            world.setBlockState(position, state.getBlockState());
        }
    }

}
