package org.techern.infinitewater;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.fml.loading.FMLCommonLaunchHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.techern.infinitewater.blocks.BlockInfinitelySpreadingWater;
import org.techern.infinitewater.items.ItemInfinitelySpreadingWaterBucket;
import org.techern.infinitewater.proxy.ClientProxy;
import org.techern.infinitewater.proxy.IProxy;
import org.techern.infinitewater.proxy.ServerProxy;

/**
 * A {@link Mod} for infinitely spreading water, similar to Minecraft Alpha builds
 *
 * @since 1.0.0
 */
@Mod(InfinitelySpreadingWaterMod.MOD_ID)
public class InfinitelySpreadingWaterMod
{
    /**
     * The mod ID
     *
     * @since 1.0.0
     */
    public static final String MOD_ID = "infinitely_spreading_water";

    /**
     * The version of the {@link InfinitelySpreadingWaterMod}
     *
     * @since 1.0.0
     */
    public static final String VERSION = "1.1.0-SNAPSHOT";

    /**
     * The {@link Logger} for {@link InfinitelySpreadingWaterMod}
     *
     * @since 1.0.0
     */
    public static Logger LOGGER = LogManager.getLogger(MOD_ID);

    /**
     * A {@link org.techern.infinitewater.proxy.IProxy} of either {@link ServerProxy} or {@link ClientProxy}
     *
     * @since 0.0.1
     */
    public static IProxy PROXY = DistExecutor.runForDist(() -> ClientProxy::new, () -> ServerProxy::new);

    /**
     * The instance of {@link BlockInfinitelySpreadingWater}
     *
     * @since 1.0.0
     */
    public static Block INFINITELY_SPREADING_WATER_BLOCK = null;

    /**
     * The instance of {@link ItemInfinitelySpreadingWaterBucket}
     *
     * @since 1.1.0
     */
    public static Item INFINITELY_SPREADING_WATER_BUCKET = null;

    public InfinitelySpreadingWaterMod() {
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::initialize);
    }

    /**
     * Called when the {@link FMLCommonSetupEvent} fires
     *
     * @param event The {@link FMLCommonSetupEvent}
     *
     * @since 1.0.0
     */
    private void initialize(final FMLCommonSetupEvent event)
    {
        LOGGER.info("Started mod " + MOD_ID);

        LOGGER.info("registering the events");
        FMLJavaModLoadingContext.get().getModEventBus().register(RegistryEvents.class);

        LOGGER.info("Registered the events");

        //GameRegistry.register(INFINITELY_SPREADING_WATER_BUCKET);FIXME Move me

        //PROXY.registerItemModelMesher(INFINITELY_SPREADING_WATER_BUCKET, 0, "infinitely_spreading_water_bucket", "inventory");FIXME move me
    }
}
