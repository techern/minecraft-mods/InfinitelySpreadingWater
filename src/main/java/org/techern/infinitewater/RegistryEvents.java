package org.techern.infinitewater;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import org.techern.infinitewater.blocks.BlockInfinitelySpreadingWater;
import org.techern.infinitewater.items.ItemInfinitelySpreadingWaterBucket;

@Mod.EventBusSubscriber(bus=Mod.EventBusSubscriber.Bus.MOD,modid="infinitely_spreading_water")
public class RegistryEvents {


    /**
     * Called when the Registry is registering all blocks
     *
     * @param event The {@link RegistryEvent} that called this
     * @since 2.0.0
     */
    @SubscribeEvent
    public static void registerBlocks(final RegistryEvent.Register<Block> event) {
        InfinitelySpreadingWaterMod.INFINITELY_SPREADING_WATER_BLOCK = new BlockInfinitelySpreadingWater();

        InfinitelySpreadingWaterMod.LOGGER.warn("Registering block");
        event.getRegistry().registerAll(InfinitelySpreadingWaterMod.INFINITELY_SPREADING_WATER_BLOCK);
        InfinitelySpreadingWaterMod.LOGGER.warn("Registered block");
    }

    /**
     * Called when the Registry is registering all items
     *
     * @param event The {@link RegistryEvent} that called this
     * @since 2.0.0
     */
    @SubscribeEvent
    public static void registerItems(final RegistryEvent.Register<Item> event) {
        InfinitelySpreadingWaterMod.INFINITELY_SPREADING_WATER_BUCKET = new ItemInfinitelySpreadingWaterBucket();
        event.getRegistry().registerAll(InfinitelySpreadingWaterMod.INFINITELY_SPREADING_WATER_BUCKET);
        InfinitelySpreadingWaterMod.LOGGER.warn("Registered item");
    }
}
