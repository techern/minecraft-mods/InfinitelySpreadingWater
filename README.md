# Infinitely-Spreading-Water
A mod for infinitely spreading water in Minecraft 1.11 and above

##What version do I use?
Our old release, 1.0 can be used in Minecraft 1.11
We are currently in the process of rewriting the entire mod (and more) for Minecraft 1.14.4

####I can't see the models (1.14 and later) - just purple and black squares!
There was an issue in Forge when updating from 1.12 - Please update your Forge version to **at least** 28.1.40

##Can I use your mod in my modpack? Can I make a video?

Yes and yes, you don't need to ask permisison :)

###Can I reupload and/or host your mod?

We'd rather you don't but, if you must, please just give credit and a backlink to either the Curse or GitLab URLs.
If your endpoint is not accessible to the general public (private host for your major server's launcher, for example) then sure, do what you want